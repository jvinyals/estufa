`timescale 1ns / 1ps

module tb_pipeline
#(
  parameter CORE_CLOCK_PERIOD = 4
)
() ;

  logic clk;
  logic res = 1;

  always #(CORE_CLOCK_PERIOD/2)
    clk = ~clk;

  initial res = 1;
  initial clk = 1;

  integer i = 0;

  always @(posedge clk) begin
    if (!i)
      i <= i+1;
    else
      res <= 0;
  end

  logic [31:0] instAddr;
  logic instLDP;
  logic instLDR, nxInstLDR;

  always_comb
    if (instLDP)
      nxInstLDR = 1;
    else
      nxInstLDR = 0;

   always_ff @(posedge clk)
     instLDR <= nxInstLDR;

  pipeline pipeline_0 (
    .clk,
    .res,

    .instAddr,
    .instLDP,
    .instLDR,
    //.instData( {8{32'h00A00093}} )    
    //.instData( { 32'h00000063, {6{32'h00A08093}}, 32'h00A00093 } )
    .instData( { 32'hFE0006E3, {14{32'h00A08093}}, 32'h00A00093 } )
    //.instData( {8{32'h00000263}} )
  );

endmodule
