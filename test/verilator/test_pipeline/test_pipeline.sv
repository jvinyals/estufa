
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "VpcStage.h"
#include "verilated.h"

int main(int argc, char **argv) {

  Verilated::commandArgs(argc, argv);

  Vpipeline *tb = new Vpipeline;

  for (int i = 0; i < 100; i++) {
    tb -> clk = 0;

    if ( tb -> instLDP ) {
      tb -> instLDR = 1;
      tb -> instData = 0xA00093;
    }
    tb -> eval();

    tb -> clk = 1;
    tb -> eval();
  }
  tb -> res = 1;
  tb -> pcFetch = 0x5555;

  tb -> pcALU     = 0xAFAF;
  tb -> injectALU = 0;

  tb -> iblock = 0;

  tb -> eval();

}
