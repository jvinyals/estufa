#define SIZE 2048

int sieve(){
  int *mem = (int*) 0x1000000000;
	
	int i;	
	for ( i=2; i < SIZE; i++)
		mem[i]=1;	

	for ( i=2; i < SIZE; i++){
		if ( mem[i] ){
			int j;
			for ( j=i; i*j<SIZE;j++)
				mem[i*j]=0;
		} 
	}
}
