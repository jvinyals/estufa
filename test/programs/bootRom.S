// 00000 : nothing
// 01000 : bootrom
// 02000 : codi interupt
// 03000 : ROOT del Translation Table (TT)
// 04000 : 2-level TT code
// 05000 : 3-level TT code
// 06000 : 2-level TT data
// 07000 : 3-level TT data
// ...
// 10000 : user code physical addres
// 11000 : ''
// 12000 : ''
// 13000 : ''
// 14000 : ''
// 15000 : ''
// ...
// 20000 : user data pysical addres
// 21000 : ''
// 22000 : ''
// 23000 : ''
// 24000 : ''
// 25000 : ''
// ...
//

#define BASE_USER_PAGES 0x03000
#define BASE_USER_CODE  0x10000 /// change
#define BASE_USER_DATA  0x20000 /// change

// BOOT ROM PAGE SETUP
//
//  CVPN0 = (BASE_USER_VIRT_CODE >> 12 + 9 + 9) & 0x01FF;
//  CVPN1 = (BASE_USER_VIRT_CODE >> 12 + 9) & 0x01FF;
//	CVPN2 = (BASE_USER_VIRT_CODE >> 12) & 0x01FF;

//  DVPN0 = (BASE_USER_VIRT_DATA >> 12 + 9 + 9) & 0x01FF;
//  DVPN1 = (BASE_USER_VIRT_DATA >> 12 + 9) & 0x01FF;
//	DVPN2 = (BASE_USER_VIRT_DATA >> 12) & 0x01FF;

#define BASE_USER_VIRT_CODE 0x0000000000
#define CVPN0 0
#define CVPN1 0
#define CVPN2 0

#define BASE_USER_VIRT_DATA 0x1000000000
#define DVPN0 0x40
#define DVPN1 0
#define DVPN2 0

#define ROOT_TT 0x03000
#define END_TT  0x08000

//the entry point
start:

#ifdef CLEAN
//invalidate translation tables
	li s1, ROOT_TT
	li s2, END_TT

L1:
	sd zero, 0(s1)
	addi s1, s1, 8
	blt s1, s2, L1
#endif

// FLAGS

// iiiU XWRV
// 0001 1011
#define UCODE_FLAGS 0x1B

// iiiU XWRV
// 0001 0111
#define UDATA_FLAGS 0x17


// [BASE_USER_PAGES + CVPN0] = 0x04, flags;
//          [0x4000 + CVPN1] = 0x05, flags;
//          [0x5000 + CVPN2] = BASE_USER_CODE>>12, flags;

// [BASE_USER_PAGES + DVPN0] = 0x06, flags;
//          [0x6000 + DVPN1] = 0x07, flags;
//          [0x7000 + DVPN2] = BASE_USER_DATA>>12, flags;
		

// Stores
#define ST_L1_CODE_DATA 0x041B
#define ST_L1_CODE_ADDR 0x3000
#define ST_L2_CODE_DATA 0x051B
#define ST_L2_CODE_ADDR 0x4000
#define ST_L3_CODE_DATA 0x101B
#define ST_L3_CODE_ADDR 0x5000

#define ST_L1_DATA_DATA 0x0617
#define ST_L1_DATA_ADDR 0x3040
#define ST_L2_DATA_DATA 0x0717
#define ST_L2_DATA_ADDR 0x6000
#define ST_L3_DATA_DATA 0x2017
#define ST_L3_DATA_ADDR 0x7000

	li s1, ST_L1_CODE_DATA	
	li s2, ST_L1_CODE_ADDR	
	sw s1, 0(s2)
	
	li s1, ST_L2_CODE_DATA	
	li s2, ST_L2_CODE_ADDR	
	sw s1, 0(s2)

	li s1, ST_L3_CODE_DATA	
	li s2, ST_L3_CODE_ADDR	
	sw s1, 0(s2)

	li s1, ST_L1_DATA_DATA	
	li s2, ST_L1_DATA_ADDR
	sw s1, 0(s2)

	li s1, ST_L2_DATA_DATA	
	li s2, ST_L2_DATA_ADDR
	sw s1, 0(s2)

	li s1, ST_L3_DATA_DATA	
	li s2, ST_L3_DATA_ADDR
	sw s1, 0(s2)

// Write CSR 1 <- ROOT_TT (0x3000)
	li s1, ROOT_TT
	csrw 1, s1

// Return
	sret
