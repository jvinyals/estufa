import sys

def print_padding(size):
    for i in range( size ):
        print("00"*16)
    
def print_empty_page():
    print_padding(256)
    
def print_code(filename):
    with open(filename) as f:
        content = f.readlines()    

    for l in content:
        print(l.split()[1])
    
    print_padding( 256-len(content) )

def print_data(filename):
    with open(filename) as f:
        content = f.readlines()
    for l in content:
        print(l, end='')
    
    print_padding( 256 - len(content) ) 


def main():
    #0x0000 - 0x1000 => empty
    print_empty_page()
    #0x1000 => bootRom
    print_code("bootRom.hex")
    #0x2000 => exceptionHandler
    print_code("exception.hex")
    #0x3000 - 0x10000 => empty
    print_padding(256*13)
    #0x10000 => user code
    print_code(sys.argv[1])
    print_padding(256*15)
    print_data(sys.argv[2])
    

if __name__ == '__main__':
    main()
