# Exception handler entry point
## When and exception occurs this code is executed

# Read the exception type
csrr t1, 2

# Compute address of exception counter
li t2, 0x3500
addw t1, t1, t2

# Load counter value
ld t2, 0(t1)

# Increment counter
addi t2, t2, 1

# Store new counter value
sd t2, 0(t1)

# Return to user
sret
