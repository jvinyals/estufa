
`timescale 1ns / 1ps

module tb_estufa
#(
  parameter CORE_CLOCK_PERIOD = 4
)
() ;

  logic clk;
  logic res = 1;

  always #(CORE_CLOCK_PERIOD/2)
    clk = ~clk;

  initial res = 1;
  initial clk = 1;

  integer i = 1;

  logic nclk;
  assign nclk = ~clk;
  always @(posedge nclk) begin
    if (i)
      i <= i-1;
    else
      res <= 0;
  end

  estufa estufa_0 (
    .clk,
    .res
  );

endmodule
