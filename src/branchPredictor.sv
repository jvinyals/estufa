
import config_pkg::*;
import branch_pkg::*;
  
 module branchPredictor
 (
	input logic clk,
	input logic res,
	
	input  logic [XLEN-1:0] pc,
	
	output logic [XLEN-1:0] predPC,
	output logic            predTake,
	output logic            predEN,
	
	input  logic            writeEnable,
	input  logic            branchTaken,
	input  logic [MLEN-1:0] branchPC,
	input  logic [XLEN-1:0] branchTargetPC
 );

  localparam integer IBPLEN = $clog2(BPLEN); // Index Branch Predictor LENgth

  logic [XLEN-1:0]   indexPC[BPLEN-1:0];
  logic [XLEN-1:0]   predict[BPLEN-1:0];
  logic              take   [BPLEN-1:0];
  logic              valid  [BPLEN-1:0];

  logic [XLEN-1:0]   nxIndexPC[BPLEN-1:0];
  logic [XLEN-1:0]   nxPredict[BPLEN-1:0];
  logic              nxTake   [BPLEN-1:0];
  logic              nxValid  [BPLEN-1:0];

  logic selEnab;
  logic selTake;
  logic [XLEN-1:0] selPred;

  assign predPC   = selPred;
  assign predTake = selTake;
  assign predEN   = selEnab;

  logic [BPLEN-1:0] matchPC;
  logic [BPLEN-1:0] writeHitSelector;

  assign writeHit = writeHitSelector == 0 ? 0 : 1;

  genvar t;
  generate
    for (t=0; t<BPLEN; t++) begin
      assign writeHitSelector[t] = indexPC[t] == branchPC & valid[t];
    end
  endgenerate

  genvar i;
  generate
    for (i=0; i<BPLEN; i++) begin
      assign matchPC[i] = indexPC[i] == pc & valid[i];
    end
  endgenerate

  logic  [BPLEN-1:0] replaced;
  logic  [BPLEN-1:0] access;
  assign access = matchPC | replaced;

  logic [BPLEN-1:0][BPLEN-1:0] lru_matrix, nx_lru_matrix;
  logic [BPLEN-1:0] lru;

  always_ff @(posedge clk) begin
    lru_matrix = nx_lru_matrix;
  end

  genvar j;
  generate
    for (j=0; j<BPLEN; j++)
      assign lru[j] = ~lru_matrix[j] ? 0 : 1;
  endgenerate

  genvar l, m;
  generate
   for (l=0; l<BPLEN; l++) 
     for (m=0; m<BPLEN; m++) 
       always_comb
       begin
         if (res) 
           nx_lru_matrix[l][m] = m < l ? 0 : 1;
         else
           nx_lru_matrix[l][m] = ( access[m] & ~lru_matrix[l][m]) | 
                                 (~access[l] &  lru_matrix[l][m]) |
                                 l == m;
       end
  endgenerate

  integer n;
  always_comb
  begin
    selEnab <= 0;
    selPred <= 32'h0;
    for (n=0; n < BPLEN; n++) begin
      if(matchPC[n]) begin
        selEnab <= valid[n];
        selTake <= take[n];
        selPred <= predict[n];
      end
    end
  end

  genvar k;
  generate
    for (k = 0; k < BPLEN; k++)
    begin : branch_predictor_control

      always_comb
      begin
        replaced[k] <= 0;

        nxIndexPC[k] <= indexPC[k];
        nxPredict[k] <= predict[k];
        nxTake[k]    <= take[k];
        nxValid[k]   <= valid[k];

        if (writeEnable)
          if (writeHit)
          begin
            if (writeHitSelector[k]) begin
              replaced[k]  <= 1;
              nxIndexPC[k] <= branchPC;
              nxPredict[k] <= branchTargetPC;
              nxTake[k]    <= branchTaken;
              nxValid[k]   <= 1;
            end
          end else begin 
            if (lru[k]) begin
              replaced[k]  <= 1;
              nxIndexPC[k] <= branchPC;
              nxPredict[k] <= branchTargetPC;
              nxTake[k]    <= branchTaken;
              nxValid[k]   <= 1;
            end
          end

        if (res) begin
          nxValid[k]   <= 0;
          nxIndexPC[k] <= {MLEN{1'b0}};
          nxPredict[k] <= {XLEN{1'b0}};
          nxTake[k]    <= 0;
        end 
      end

      always_ff @(posedge clk)
      begin
        indexPC[k] = nxIndexPC[k];
        predict[k] = nxPredict[k];
        take[k]    = nxTake[k];
        valid[k]   = nxValid[k];
      end

    end
  endgenerate
 
 endmodule
