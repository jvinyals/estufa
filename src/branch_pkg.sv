
package branch_pkg;

  import config_pkg::*;

  typedef struct {
    logic valid;
    logic taken;
    logic [XLEN-1:0] pc;
    logic [XLEN-1:0] takenPC;
    logic [XLEN-1:0] implicitPC;
  } branchPrediction_t ;

  typedef struct {
    logic miss;
    logic taken;
    logic [XLEN-1:0] pc;
    logic [XLEN-1:0] takenPC;
    logic [XLEN-1:0] implicitPC;
  } evaluatedBranch_t ;

endpackage
