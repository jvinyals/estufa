
import config_pkg::*;
import branch_pkg::*;
import tlb_pkg::*;


// SATP: 0
//
// VPN0 VPN1 VPN2 OFFSET
//    1
//
// PPN0 PPN1 PPN2 OFFSET

//    +++++++++++++++
// 0  |           |F|
//    +-----------+-|
// 1  |        15 |F|
//    +-----------+-|
// 2  |           |!|
//    +-----------+-|
// 3  |           |F|
//    +-----------+-|
// 4  |           |F|
//    +-----------+-|
// 5  |           |F|
//    +-----------+-+
  
//    +++++++++++++++
// 15 |           |F|
//    +-----------+-|
// 16 |        15 |F|
//    +-----------+-|
// 17 |           |F|
//    +-----------+-|
// 18 |           |F|
//    +-----------+-|
// 19 |           |F|
//    +-----------+-|
// 20 |           |F|
//    +-----------+-+
//
 module translationLookasideBuffer
 (
	input logic clk,
	input logic res,
	
	input  logic [XLEN-1:0] vaddr,
	
  output logic [MLEN-1:0] paddr,    // Physical addres
  output tlb_flags_t      pflags,   // Page flags
  output logic miss,                // TLB miss
  output logic pf,                  // Page translation fault

  output logic ptp,                 // Page translation petition
  output logic [XLEN-1:0] ptAddr,   // Page translation address
  input  logic ptr,                 // Page translation response
  input  logic ptf,                 // Page translation fault
  input  logic [XLEN-1:0] ptData    // Page translated
 );

 /**
 * TODO:  FINISH TLB
 *  - find a proper way to communicate the miss and fault to the stage.
 *  - create an interface (ptBus) for the communication with the page walker.
 *  - verify the wellbeeing of the module
 */

  /**
  * TODO: CREATE A PAGE WALKER
  *  - design the page walker interface considering:
  *    + has to use the ptBus
  *    + has to use the roBus
  *    + has to switch between the L1I & L1D
  */

  /**
  * TODO: CREATE A GRADUATION LIST, AND RENAMING LOGIC
  *  - has to contain the info:
  *    + Valid
  *    + Ready
  *    + PC
  *    + Branch prediction correction info
  *    + Renaming branch
  *    + Destination register 
  *    + Destination data
  *    + Exception info
  */

  /**
  * TODO: CREATE AN INSTRUCTION WINDOW
  *  - has to cintain wake logic
  *  - has to contain sleep logic (for tlb&cache misses)
  *  - has to contain kill logic
  *  - has to contain instruction aquisition logic
  */

  /**
  * TODO: CREATE A PICKER
  *  - has to pick an instruction that:
  *    + can be executed
  *    + inside the tail and head
  *    + and its the younges among the possibles
  */

  logic [VPNLEN-1:0] VPN [TLBLEN-1:0];
  logic [PPNLEN-1:0] PPN [TLBLEN-1:0];
  tlb_flags_t      flags [TLBLEN-1:0];

  logic [VPNLEN-1:0] nxVPN [TLBLEN-1:0];
  logic [PPNLEN-1:0] nxPPN [TLBLEN-1:0];
  tlb_flags_t      nxFlags [TLBLEN-1:0];


  logic selHit;
  logic [TLBLEN-1:0] hitMask;
  tlb_flags_t selFlags;
  logic [PPNLEN-1:0] selPPN;

  assign miss = ~selHit;
  
  assign paddr = {selPPN, vaddr[3:0]};
  assign pflags = selFlags;


  assign ptp  = ~selHit;
  assign ptAddr = vaddr;


  genvar i;
  generate
    for (i=0; i<TLBLEN; i++) begin
      assign hitMask[i] = VPN[i] == vaddr[31:4] & flags[i].V;
    end
  endgenerate

  logic  [TLBLEN-1:0] replaced;
  logic  [TLBLEN-1:0] access;
  assign access = hitMask | replaced;

  logic [TLBLEN-1:0][TLBLEN-1:0] lru_matrix, nx_lru_matrix;
  logic [TLBLEN-1:0] lru;

  always_ff @(posedge clk) begin
    lru_matrix = nx_lru_matrix;
  end

  genvar j;
  generate
    for (j=0; j<BPLEN; j++)
      assign lru[j] = ~lru_matrix[j] ? 0 : 1;
  endgenerate

  genvar l, m;
  generate
   for (l=0; l<BPLEN; l++) 
     for (m=0; m<BPLEN; m++) 
       always_comb
       begin
         if (res) 
           nx_lru_matrix[l][m] = m < l ? 0 : 1;
         else
           nx_lru_matrix[l][m] = ( access[m] & ~lru_matrix[l][m]) | 
                                 (~access[l] &  lru_matrix[l][m]) |
                                 l == m;
       end
  endgenerate

  integer n;
  always_comb
  begin
    selHit <= 0;
    selPPN <= {PPNLEN{1'b0}};
    for (n=0; n < TLBLEN; n++) begin
      if(hitMask[n]) begin
        selHit <= flags[n].V;
        selPPN <= PPN[n];
        selFlags <= flags[n];
      end
    end
  end

  genvar k;
  generate
    for (k = 0; k < TLBLEN; k++)
    begin : branch_predictor_control

      always_comb
      begin
        replaced[k] <= 0;

        nxVPN[k]   <= VPN[k];
        nxPPN[k]   <= PPN[k];
        nxFlags[k] <= flags[k];

        if (~ptf & ptr & lru[k]) begin // Page translation response without page translation fault
          replaced[k] <= 1;
          nxVPN[k]    <= vaddr[31:4];
          nxPPN[k]    <= ptData[23:8];
          {>>{nxFlags[k]}}  <= ptData[7:0];
        end

        if (res) begin
          nxVPN[k]    <= {VPNLEN{1'b0}};
          nxPPN[k]    <= {PPNLEN{1'b0}};
          {>>{nxFlags[k]}}  <= 8'h00;
        end 
      end

      always_ff @(posedge clk)
      begin
        VPN[k] <= nxVPN[k];
        PPN[k] <= nxPPN[k];
        flags[k] <= nxFlags[k];
      end

    end
  endgenerate
 
 endmodule
