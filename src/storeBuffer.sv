
import config_pkg::*;

module storeBuffer
(
  input logic clk,
  input logic res,

  input  logic                     glen,
  input  logic [$clog2(GLLEN)-1:0] glid,

  output logic full,
  output logic empty,

  input  logic                en_i,
  input  logic    [$clog2(GLLEN)-1:0] glid_i,
  input  logic     [MLEN-1:0] addr_i,
  input  logic [(XLEN/8)-1:0] mask_i,
  input  logic     [XLEN-1:0] data_i,

  output logic                en_o,
  output logic    [$clog2(GLLEN)-1:0] glid_o,
  output logic     [MLEN-1:0] addr_o,
  output logic [(XLEN/8)-1:0] mask_o,
  output logic     [XLEN-1:0] data_o
);

  parameter BUFFLEN = 16;

  logic [$clog2(BUFFLEN)-1:0] tail, nxTail;


  typedef struct{
    logic            valid;
    logic  [MLEN-1:0] addr;
    logic  [XLEN-1:0] data;
    logic  [XLEN-1:0] wren;
    logic [GLLEN-1:0] glid;
  } store_buff_entry_t ;

  store_buff_entry_t   buff[BUFFLEN:0];
  store_buff_entry_t nxBuff[BUFFLEN-1:0];

  logic  enable_drain; // When we drain from the last entry of the csr table.
  assign enable_drain = buff[0].valid && buff[0].glid == glid && glen;

  assign full  = tail == BUFFLEN ;
  assign empty = tail == 0;

  assign en_o   = enable_drain;
  assign addr_o = buff[0].addr;
  assign data_o = buff[0].data;
  assign mask_o = buff[0].wren;
  assign glid_o = buff[0].glid;

  logic [$clog2(BUFFLEN)-1:0] write_tail;

  always_comb
  begin
    if (enable_drain) write_tail = tail - 1;
    else              write_tail = tail;
  end

  always_comb
  begin
    if (enable_drain) begin 
      if (en_i) nxTail <= write_tail + 1;
      else      nxTail <= write_tail;
    end else begin
      if (en_i) nxTail <= tail + 1;
      else      nxTail <= tail;
    end
    if (res) nxTail <= 0;
  end

  always_ff @(posedge clk)
    tail = nxTail;

  genvar i;
  generate 

    for(i=0; i<BUFFLEN; i++) begin
      always_comb
      begin
        if (enable_drain) begin
          nxBuff[i] <= buff[i+1];
        end else begin
          nxBuff[i] <= buff[i];
        end

        if (en_i && i == write_tail) begin
          nxBuff[i].valid <= 1;
          nxBuff[i].addr  <= addr_i;
          nxBuff[i].data  <= data_i;
          nxBuff[i].wren  <= mask_i;
          nxBuff[i].glid  <= glid_i;
        end

        if (res) begin 
          nxBuff[i].valid <= 0;
          nxBuff[i].addr  <= 0;
          nxBuff[i].data  <= 0;
          nxBuff[i].wren  <= 0;
          nxBuff[i].glid  <= 0;
        end
      end
    end
  endgenerate

  always_ff @(posedge clk) 
  begin
    buff[BUFFLEN].valid = 0;
    buff[BUFFLEN].addr  = 0;
    buff[BUFFLEN].data  = 0;
    buff[BUFFLEN].wren  = 0;
    buff[BUFFLEN].glid  = 0;

    buff[BUFFLEN-1:0] = nxBuff;
  end

endmodule
