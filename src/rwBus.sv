import config_pkg::*;

interface rwBus ();
  logic  [MLEN-1:0] addr;
  logic             ldp;
  logic             ldr;
  logic [MBLEN-1:0] ldData;
  logic             stp;
  logic             str;
  logic [MBLEN-1:0] stData;

  // Lower level perspective
  modport ll (output addr, ldp, stp, stData, input ldr, str, ldData);

  // Higher level perspective
  modport hl (input addr, ldp, stp, stData, output ldr, str, ldData);

endinterface

