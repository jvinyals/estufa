add wave -noupdate -divider {BP - Prediction}
add wave -noupdate -label Valid /tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predEN
add wave -noupdate -label PC -radix hexadecimal /tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predPC
add wave -noupdate -label Take /tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predTake

add wave -noupdate -divider {BP - Correction}
add wave -noupdate -label Valid -radix binary /tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/writeEnable
add wave -noupdate -label Index -radix hexadecimal {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/branchPC}
add wave -noupdate -label Traget -radix hexadecimal {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/branchTargetPC}
add wave -noupdate -label Take -radix binary /tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/branchTaken

add wave -noupdate -divider {BP - State}

add wave -noupdate -radix hexadecimal  -group {Line 0} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[0]}
add wave -noupdate -radix hexadecimal  -group {Line 0} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[0]}
add wave -noupdate -group {Line 0} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[0]}
add wave -noupdate -group {Line 0} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[0]}

add wave -noupdate -radix hexadecimal  -group {Line 1} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[1]}
add wave -noupdate -radix hexadecimal  -group {Line 1} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[1]}
add wave -noupdate  -group {Line 1} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[1]}
add wave -noupdate  -group {Line 1} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[1]}

add wave -noupdate -radix hexadecimal  -group {Line 2} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[2]}
add wave -noupdate -radix hexadecimal  -group {Line 2} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[2]}
add wave -noupdate  -group {Line 2} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[2]}
add wave -noupdate  -group {Line 2} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[2]}

add wave -noupdate -radix hexadecimal  -group {Line 3} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[3]}
add wave -noupdate -radix hexadecimal  -group {Line 3} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[3]}
add wave -noupdate  -group {Line 3} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[3]}
add wave -noupdate  -group {Line 3} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[3]}

add wave -noupdate -radix hexadecimal  -group {Line 4} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[4]}
add wave -noupdate -radix hexadecimal  -group {Line 4} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[4]}
add wave -noupdate  -group {Line 4} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[4]}
add wave -noupdate  -group {Line 4} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[4]}

add wave -noupdate -radix hexadecimal  -group {Line 5} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[5]}
add wave -noupdate -radix hexadecimal  -group {Line 5} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[5]}
add wave -noupdate  -group {Line 5} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[5]}
add wave -noupdate  -group {Line 5} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[5]}

add wave -noupdate -radix hexadecimal  -group {Line 6} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[6]}
add wave -noupdate -radix hexadecimal  -group {Line 6} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[6]}
add wave -noupdate  -group {Line 6} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[6]}
add wave -noupdate  -group {Line 6} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[6]}

add wave -noupdate -radix hexadecimal  -group {Line 7} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[7]}
add wave -noupdate -radix hexadecimal  -group {Line 7} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[7]}
add wave -noupdate  -group {Line 7} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[7]}
add wave -noupdate  -group {Line 7} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[7]}

add wave -noupdate -radix hexadecimal  -group {Line 8} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[8]}
add wave -noupdate -radix hexadecimal  -group {Line 8} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[8]}
add wave -noupdate  -group {Line 8} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[8]}
add wave -noupdate  -group {Line 8} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[8]}

add wave -noupdate -radix hexadecimal  -group {Line 9} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[9]}
add wave -noupdate -radix hexadecimal  -group {Line 9} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[9]}
add wave -noupdate  -group {Line 9} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[9]}
add wave -noupdate  -group {Line 9} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[9]}

add wave -noupdate -radix hexadecimal  -group {Line 10} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[10]}
add wave -noupdate -radix hexadecimal  -group {Line 10} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[10]}
add wave -noupdate  -group {Line 10} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[10]}
add wave -noupdate  -group {Line 10} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[10]}

add wave -noupdate -radix hexadecimal  -group {Line 11} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[11]}
add wave -noupdate -radix hexadecimal  -group {Line 11} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[11]}
add wave -noupdate  -group {Line 11} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[11]}
add wave -noupdate  -group {Line 11} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[11]}

add wave -noupdate -radix hexadecimal  -group {Line 12} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[12]}
add wave -noupdate -radix hexadecimal  -group {Line 12} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[12]}
add wave -noupdate  -group {Line 12} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[12]}
add wave -noupdate  -group {Line 12} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[12]}

add wave -noupdate -radix hexadecimal  -group {Line 13} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[13]}
add wave -noupdate -radix hexadecimal  -group {Line 13} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[13]}
add wave -noupdate  -group {Line 13} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[13]}
add wave -noupdate  -group {Line 13} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[13]}

add wave -noupdate -radix hexadecimal  -group {Line 14} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[14]}
add wave -noupdate -radix hexadecimal  -group {Line 14} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[14]}
add wave -noupdate  -group {Line 14} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[14]}
add wave -noupdate  -group {Line 14} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[14]}

add wave -noupdate -radix hexadecimal  -group {Line 15} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/indexPC[15]}
add wave -noupdate -radix hexadecimal  -group {Line 15} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/predict[15]}
add wave -noupdate  -group {Line 15} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/take[15]}
add wave -noupdate  -group {Line 15} {/tb_estufa/estufa_0/pipeline_0/fetchStage_0/branchPredictor_0/valid[15]}

update
