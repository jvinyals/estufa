onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {Page Walker State}
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/res
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/pipeline_0/pageWalker_0/satp
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/src
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/state
add wave -noupdate -divider {ITLB Bus}
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/itlb/ptp
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/pipeline_0/pageWalker_0/itlb/addr
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/itlb/ptr
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/itlb/ptf
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/pipeline_0/pageWalker_0/itlb/data
add wave -noupdate -divider {DTLB Bus}
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/dtlb/ptp
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/pipeline_0/pageWalker_0/dtlb/addr
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/dtlb/ptr
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/dtlb/ptf
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/pipeline_0/pageWalker_0/dtlb/data
add wave -noupdate -divider {PW Memory Bus}
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/pipeline_0/pageWalker_0/bus/addr
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/bus/ldp
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/bus/ldr
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/pipeline_0/pageWalker_0/bus/ldData
add wave -noupdate -divider {Memory Switch State}
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/pageWalker_0/clk
add wave -noupdate /tb_estufa/estufa_0/ms/src
add wave -noupdate /tb_estufa/estufa_0/ms/state
add wave -noupdate -divider {Memory Switch Bus}
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/mainBUS/addr
add wave -noupdate /tb_estufa/estufa_0/ms/mainBUS/ldp
add wave -noupdate /tb_estufa/estufa_0/ms/mainBUS/ldr
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/mainBUS/ldData
add wave -noupdate /tb_estufa/estufa_0/ms/mainBUS/stp
add wave -noupdate /tb_estufa/estufa_0/ms/mainBUS/str
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/mainBUS/stData
add wave -noupdate -divider {MS Inst Bus}
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/instBUS/addr
add wave -noupdate /tb_estufa/estufa_0/ms/instBUS/ldp
add wave -noupdate /tb_estufa/estufa_0/ms/instBUS/ldr
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/instBUS/ldData
add wave -noupdate -divider {MS Data Bus}
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/dataBUS/addr
add wave -noupdate /tb_estufa/estufa_0/ms/dataBUS/ldp
add wave -noupdate /tb_estufa/estufa_0/ms/dataBUS/ldr
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/dataBUS/ldData
add wave -noupdate /tb_estufa/estufa_0/ms/dataBUS/stp
add wave -noupdate /tb_estufa/estufa_0/ms/dataBUS/str
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/dataBUS/stData
add wave -noupdate -divider {MS Page Bus}
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/pageBUS/addr
add wave -noupdate /tb_estufa/estufa_0/ms/pageBUS/ldp
add wave -noupdate /tb_estufa/estufa_0/ms/pageBUS/ldr
add wave -noupdate -radix hexadecimal /tb_estufa/estufa_0/ms/pageBUS/ldData
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {573994 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {533061 ps} {661797 ps}
