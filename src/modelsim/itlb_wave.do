onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider TLB
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/fetchStage_0/itlb/vaddr
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/fetchStage_0/itlb/miss
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/fetchStage_0/itlb/paddr
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/fetchStage_0/itlb/pflags
add wave -noupdate -divider {TLB Data}
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/fetchStage_0/itlb/VPN
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/fetchStage_0/itlb/PPN
add wave -noupdate /tb_estufa/estufa_0/pipeline_0/fetchStage_0/itlb/flags
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1206211 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1155889 ps} {1240330 ps}
