
import config_pkg::*;

module mainMemory
(
  input  logic clk,
  input  logic res,

  rwBus.hl bus
);

  // TOTAL MEMORY: 2 ^ 20 = 1024 * 1024
  // EACH LINE: MBLEN => NUMBER OF LINES: 2^20/(MBLEN*XLEN/8)
  localparam integer NLINES = ((2**MLEN)/(MBLEN/8));

  logic [MBLEN-1:0] ROM   [0:NLINES-1];
  logic [MBLEN-1:0] MEM   [0:NLINES-1];
  logic [MBLEN-1:0] MEMFF [0:NLINES-1];

  // memory initialization

  integer i;
  initial
  begin
    for(i=0;i!=NLINES;i=i+1)
      MEM[i] = {MBLEN{1'b0}}; ;

    $readmemh("../memory.mem",ROM);
  end

  logic [10:0] eff_addr;
  assign eff_addr = bus.addr[16:6]; // the bus.addr[5:0] range contains the offset among the line
  
  assign bus.ldData = MEM[eff_addr];

  always_ff @(posedge clk)
    MEM = MEMFF;

  genvar j;
  generate
    for (j=0;j!=NLINES;j++) begin : memory_block_control
      always_comb
      begin 
        MEMFF[j] <= MEM[j];
        if ( eff_addr == j & bus.stp ) MEMFF[j] <= bus.stData;
        if ( res )                     MEMFF[j] <= ROM[j];
      end
    end
  endgenerate

  
  // State & bus control 
  typedef enum { IDLE, BUSSY, ACK} state_t;
  state_t state, nxState;

  logic nxLdr, nxStr;
  logic ldr, str;
  assign bus.ldr = ldr;
  assign bus.str = str;

  always_ff @(posedge clk)
  begin
    state = nxState;
    ldr = nxLdr;
    str = nxStr;
  end

  always_comb
  begin

    nxState <= state;
    nxLdr   <= 0;
    nxStr   <= 0; 

    case(state) 
      IDLE: begin
        if ( bus.ldp ) begin           // If we recive a load petition we respond it
          nxState <= BUSSY;
          nxLdr   <= 1;
        end else if ( bus.stp ) begin  // If we recive a store petition we respond it
          nxState <= BUSSY;
          nxStr   <= 1;
        end
      end

      BUSSY: begin  // When BUSSY (after holding the signals for one cycle) we set everything to 0
        nxState <= ACK;
        nxLdr   <= 0;
        nxStr   <= 0;
      end

      default: // ACK
        if ( ~ (bus.ldp | bus.stp ) ) begin
          nxState <= IDLE;
        end

    endcase

    if (res) begin
      nxState <= IDLE;
      nxLdr   <= 0;
      nxStr   <= 0;
    end
  end

endmodule
