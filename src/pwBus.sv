
import config_pkg::*;

interface pwBus ();

  logic ptp;                 // Page translation petition
  logic [XLEN-1:0] addr;     // Page translation address (addres to be translated)
  logic ptr;                 // Page translation response
  logic ptf;                 // Page translation fault
  logic [XLEN-1:0] data;     // Page translated

  // Lower level perspective
  modport ll (output ptp, addr, input ptr, ptf, data);

  // Higher level perspective
  modport hl (input ptp, addr, output ptr, ptf, data);

endinterface

