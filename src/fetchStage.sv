
import config_pkg::*;
import branch_pkg::*;
import instruction_pkg::codedInstruction_t;
import tlb_pkg::*;

module fetchStage
(
  input  logic clk,
  input  logic res,

  input  logic su,  // Supervisor/User mode from csr4

  // Current fetched instruction
  output codedInstruction_t instruction_o,

  // Pipeline control
  // ----------------
  // Stalling:
  input  logic stall_i,
  // Update Branch Predictor
  input  evaluatedBranch_t  eb,
  //
  // Killing:
  // Interruption
  input  logic intr,

  input  logic sret,
  input  logic [XLEN-1:0] csr0_data,

  // Upper level interface
  // ---------------------
  roBus.ll instBUS,

  // Page Walker interface
  // ---------------------
  input logic flush_tlb,
  pwBus.ll pageBUS

);

  logic [XLEN-1:0] vpc;
  logic [MLEN-1:0] ppc;

  localparam BOOT_PC = 32'h0000_1000 ;
  localparam INTR_PC = 32'h0000_2000 ;

  // Program Counter Control
  // -----------------------
  //
  logic  [XLEN-1:0] pc, nxpc;
  assign vpc = pc;

  always_ff @(posedge clk)
    pc = nxpc;

  logic [XLEN-1:0] pc4;
  assign pc4= pc + 4;

  // Instruction Register Control
  // ----------------------------
  //
  logic valid;
  logic [ILEN-1:0]   nxir;
  branchPrediction_t bp;

  logic [MLEN-1:0] tlb_pc; 
  tlb_flags_t tlb_flags;
  logic pageFault;


  assign instruction_o.valid = valid;
  assign instruction_o.inst  = nxir;
  assign instruction_o.bp    = bp;
  assign instruction_o.su    = su;
  assign instruction_o.vpc   = vpc;
  assign instruction_o.excep = pageFault | tlb_flags.X == 0 | tlb_flags.R == 0 ? IPF_EXCEP : 0;

  // Branch prediction control
  //
  logic bpen; assign bp.valid      = bpen;
  logic bptb; assign bp.taken      = bptb;
              assign bp.pc         = vpc;

  logic [MLEN-1:0] tlb_ppc;

  logic [XLEN-1:0] bppc;
  assign bp.takenPC    = bppc;
  assign bp.implicitPC = pc4;

  logic  en_itlb;
  assign en_itlb = ~su;

  logic  res_tlb;
  assign res_tlb = res | flush_tlb;

  translationLookasideBuffer itlb (
    .clk,
    .res(res_tlb),

    .en(en_itlb),
	
	  .vaddr(vpc),
	
    .paddr(tlb_ppc),
    .pflags(tlb_flags),
    .miss(tlb_miss),
    .pf(pageFault),

    .bus(pageBUS)
  );

  // Set both to zero bc. not implemented yet
  //
  logic            itblMiss;
  logic [31:0]     itlbAddr;
  assign itlbMiss = 1'b0;
  assign itlbAddr = 32'h0000_0000;

  logic            instMiss;
  logic [ILEN-1:0] instCache;

  logic cache_miss;

  // We stop the fetch procedure if: we recive a signal 
  // informing that the following stage has stoped (stall_i);
  // if a read petition is going on from the cashe; or if 
  // the tlb page walker is active.
  logic  stall;
  //             nxstage   i-cache miss  i-tlb miss
  assign stall = stall_i | cache_miss  | tlb_miss;
  //                                          Petition          Response
  // We need to kill the current instruction (ie inject a 
  // NOP instruction) when: the current instruction gets 
  // stalled; the ALU evaluates that we mispredicted a
  // branch; the pipeline has issued an interruption; or 
  // when we have a reset signal.
  logic  kill;
  // Maybe not needed (done in pipeline control @ pipeline.sv)
  assign kill = eb.miss | intr | res;

  always_comb
  begin

    nxpc <= pc4;

    // If unit is stalled do not change pc
    if (stall)
      nxpc <= pc;

    // If branch predict and predict take.
    if (bpen & bptb)
      nxpc <= bppc;

    /** 
      If branch miss-predict and the alu predicts:
       - not taken: nxpc = original pc
       -     taken: nxpc = alu computed pc
    **/
    if (eb.miss)
      nxpc <= eb.taken ? eb.takenPC : eb.implicitPC; 

    // When interrupt drop other PC.
    if (intr)
      nxpc <= INTR_PC;

    if (sret)
      nxpc <= csr0_data;

    // Drop everything @ reset.
    if (res)
      nxpc <= BOOT_PC;

  end


  // move to the imem
  // assign addr = { pc[XLEN-1:6], 6'h00 }; // 6 bits becouse 2^6 = 64bytes = 512bits
  
  
  assign ppc = su ? vpc[MLEN-1:0] : tlb_ppc;

  logic  en_imem;
  assign en_imem = ~tlb_miss;

  icache_t imem(
    .clk(clk),
	 .res(res),

   .en(en_imem),
	 
	 .addr(ppc),
	 .odata(instCache),
	 
	 .miss(cache_miss),
	 
   .bus(instBUS)
  );
  
  //             IDLE,  Load Petition, Load Abort
  typedef enum { IDLE,            LDP,        LDA } instBUS_state_t;
  instBUS_state_t instBUS_state, nxInstBUS_state;

  branchPredictor branchPredictor_0(
    .clk(clk),
    .res(res),

    // Predictor signals
    .pc(vpc),
    
    .predPC(bppc),
    .predTake(bptb),
    .predEN(bpen),

    // Update signals
    .writeEnable(eb.miss),
    .branchTaken(eb.taken),
    .branchPC(eb.pc),
    .branchTargetPC(eb.takenPC)
  );

  logic [15:0][XLEN-1:0] bypassCache;
  assign bypassCache = instBUS.ldData;

  always_comb
  begin
    valid = ~(kill | stall);
    nxir = instCache;
  end

endmodule
