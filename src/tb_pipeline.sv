`timescale 1ns / 1ps

module tb_pipeline
#(
  parameter CORE_CLOCK_PERIOD = 4
)
() ;

  logic clk;
  logic res = 1;

  always #(CORE_CLOCK_PERIOD/2)
    clk = ~clk;

  initial res = 1;
  initial clk = 1;

  integer i = 0;

  always @(posedge clk) begin
    if (!i)
      i <= i+1;
    else
      res <= 0;
  end

  logic [31:0] instAddr;
  logic instLDP;
  logic instLDR, nxInstLDR;

  logic dataLDP;
  logic dataLDR, nxDataLDR;
  logic dataSTR, nxDataSTR;

  roBus instBUS();
  rwBus dataBUS();
  roBus tlbwBUS();

  assign tlbwBUS.ldr = 0;

  assign dataBUS.ldData = 512'h403f3e3d3c3b3a393837363534333231302f2e2d2c2b2a292827262524232221201f1e1d1c1b1a191817161514131211100f0e0d0c0b0a090807060504030201;
  assign dataBUS.ldr = dataLDR;
  assign dataBUS.str = dataSTR;
  always_comb
    if (dataBUS.ldp)
      nxDataLDR = 1;
    else
      nxDataLDR = 0;
  always_comb
    if (dataBUS.stp)
      nxDataSTR = 1;
    else
      nxDataSTR = 0;

   always_ff @(posedge clk) dataLDR <= nxDataLDR;
   always_ff @(posedge clk) dataSTR <= nxDataSTR;

  //assign instBUS.ldData =  { 32'hFE0006E3, {14{32'h00A08093}}, 32'h00A00093 } ;
  //assign instBUS.ldData = { {10{32'h00000000}}, 32'hfea506e3, 32'h00458593,32'h00358593,32'h00258593,32'h00158593,32'h00a50593} ;

  // Count
  //assign instBUS.ldData = { {320'h0}, 192'hfea508e30045859300358593002585930015859300a50593 };

  // Memory load
  assign instBUS.ldData = { 416'h0, 96'h00200e030000138300000303 };

  assign instBUS.ldr = nxInstLDR;
  always_comb
    if (instBUS.ldp)
      nxInstLDR = 1;
    else
      nxInstLDR = 0;

   always_ff @(posedge clk)
     instLDR <= nxInstLDR;

  pipeline pipeline_0 (
    .clk,
    .res,

    .instBUS(instBUS),
    .dataBUS(dataBUS),
    .pageBUS(tlbwBUS)
  );

endmodule
