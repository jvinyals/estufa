import config_pkg::*;

interface roBus ();
  logic  [MLEN-1:0] addr;
  logic             ldp;
  logic             ldr;
  logic [MBLEN-1:0] ldData;

  // Lower level perspective
  modport ll (output addr, ldp, input ldr, ldData);

  // Higher level perspective
  modport hl (input addr, ldp, output ldr, ldData);

endinterface

