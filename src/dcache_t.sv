
import config_pkg::*;

module dcache_t
(
  input  logic clk,
  input  logic res,

  input  logic [MLEN-1:0] addr,

  input  logic                   en, // Cache enabled
  input  logic [(XLEN/8)-1:0]  wren, // Write Enable mask for each byte
  input  logic     [XLEN-1:0] idata, // Data to write
  output logic     [XLEN-1:0] odata, // Data readed

  output logic miss,             // Missed the data

  rwBus.ll bus                   // Bus of data ot higher level
);

  localparam integer nCacheLines = DCLEN;
  localparam integer nBitsIndex  = $clog2(nCacheLines);

  localparam integer nBytesXLEN  = XLEN/8;
  localparam integer nMaskedBits = $clog2(nBytesXLEN);

  localparam integer nBytesLine  = DCLLEN / 8;
  localparam integer nBitsOffset = $clog2(nBytesLine) - nMaskedBits;

  localparam integer nBitsTag    = MLEN - nBitsIndex - nBitsOffset - nMaskedBits;

  localparam integer offset_b = nMaskedBits;
  localparam integer offset_e = offset_b + nBitsOffset - 1;

  localparam integer index_b = offset_e + 1;
  localparam integer index_e = index_b  + nBitsIndex -1;

  localparam integer tag_b = index_e + 1;
  localparam integer tag_e = tag_b   + nBitsTag -1;

  logic                       valid [DCLEN-1:0];
  logic                       dirty [DCLEN-1:0];
  logic      [nBitsTag-1:0]    tags [DCLEN-1:0];
  logic        [DCLLEN-1:0]    data [DCLEN-1:0];

  logic                     nxValid [DCLEN-1:0];
  logic                     nxDirty [DCLEN-1:0];
  logic      [nBitsTag-1:0]  nxTags [DCLEN-1:0];
  logic        [DCLLEN-1:0]  nxData [DCLEN-1:0];

  logic                selHit;
  logic                selDirty;
  logic [nBitsTag-1:0] selTag;
  logic     [XLEN-1:0] selData;

  logic              [nBitsIndex-1:0] index;
  logic             [nBitsOffset-1:0] offset;
  logic [(DCLLEN/XLEN)-1:0][XLEN-1:0] selLine;

  logic [(XLEN/8)-1:0][7:0] decompIData; // Input data decomposed in bytes
  assign decompIData = idata;

  logic [(XLEN/8)-1:0][7:0] decompSData; // Selected data decomposed in bytes
  assign decompSData = selData;

  logic [(XLEN/8)-1:0][7:0] maskedData; // Masked data
  //
  assign odata = selData;

  assign offset = addr[offset_e:offset_b];
  assign index  = addr[ index_e: index_b];

  assign selTag    =  tags[index];
  assign selDirty  = dirty[index];
  assign selLine   =  data[index];

  assign selData   = selLine[offset];

  genvar j;
  generate
    for (j = 0; j < XLEN/8; j++) begin
      assign maskedData[j] = wren[j] ? decompIData[j] : decompSData[j] ;
    end
  endgenerate

  always_comb
  begin
    selHit = selTag == addr[tag_e:tag_b] && valid[index];
  end
  assign miss = ~selHit & en;

  logic [DCLEN-1:0] lineSelector;
  assign lineSelector = 1 << index;

  genvar i;
  generate
    for (i = 0; i < DCLEN; i++) begin: data_cache_line_control
      always_comb
      begin
        nxValid[i] <= valid[i];
        nxDirty[i] <= dirty[i];
        nxTags[i]  <= tags[i];
        nxData[i]  <= data[i];

        if (lineSelector[i]) begin
          if (selHit && wren)  begin
            nxDirty[i] <= 1;
            nxData[i]  <= maskedData;
          end else if (bus.ldr) begin
            nxValid[i] <= 1;
            nxDirty[i] <= 0;
            nxTags[i]  <= addr[tag_e:tag_b];
            nxData[i]  <= bus.ldData;
          end
        end

        if (res) begin
          nxValid[i] <= 0;
          nxDirty[i] <= 0;
          nxTags[i]  <= 0;
          nxData[i]  <= 0;
        end
      end

      always_ff @(posedge clk)
      begin
        valid[i] = nxValid[i];
        dirty[i] = nxDirty[i];
        tags[i]  = nxTags[i];
        data[i]  = nxData[i];
      end
    end
  endgenerate
  
  // Bus controller
  // --------------
  //
  typedef enum { IDLE, LDP, STP } dataBUS_state_t;
  dataBUS_state_t state, nxstate;

  logic ldp, nxldp;
  logic stp, nxstp;

  assign bus.addr = addr;
  assign bus.ldp  = ldp;
  assign bus.stp  = stp;
  assign bus.stData = selLine;

  always_ff @(posedge clk) 
  begin
    state = nxstate;
    ldp   = nxldp;
    stp   = nxstp;
  end

  always_comb
  begin

    nxstate <= state;
    nxldp   <= ldp;
    nxstp   <= stp;

    case(state)
      IDLE: begin
        if ( miss ) 
          if (selDirty) begin
            nxstate <= STP;
            nxstp   <= 1;
          end else begin
            nxstate <= LDP;
            nxldp   <= 1;
          end
      end
      LDP:  begin
        if(bus.ldr) begin
          nxstate <= IDLE;
          nxldp   <= 0;
        end
      end
      STP:  begin
        if(bus.str) begin
          nxstate <= LDP;
          nxstp   <= 0;
          nxldp   <= 1;
        end
      end
    endcase

    if(res) begin
      nxstate <= IDLE;
      nxstp   <= 0;
      nxldp   <= 0;
    end
  end

endmodule
