
 
package instruction_pkg;

  import config_pkg::*;
  import branch_pkg::*;

  typedef struct {
    logic valid;
    logic [ILEN-1:0]  inst;
    branchPrediction_t bp;

    logic su;

    logic [XLEN-1:0] excep;
    logic [XLEN-1:0] vpc;
  } codedInstruction_t;

  typedef struct {
    logic valid;

    logic [$clog2(GLLEN)-1:0] glid;

    logic su;

    logic isRr; // is Register-Register
    logic isRrw; // is Register-Register
    logic isIm; // is Immedate
    logic isLd; // is Load
    logic isSt; // is Store
    logic isBr; // is Branch
    logic isMul;
    logic isSys;

	  logic [6:0] func7; //
	  logic [2:0] func3; //

    logic [$clog2(RFLEN)-1:0] src1Addr;
    logic [$clog2(RFLEN)-1:0] src2Addr;
    logic [$clog2(RFLEN)-1:0]  dstAddr; // Destination register

    logic [XLEN-1:0] excep;
    logic [XLEN-1:0] vpc;
  } renamedInstruction_t; // TODO: change name to decoded instruction


  typedef struct {
    logic valid;

    logic [$clog2(GLLEN)-1:0] glid;

    logic su;   // Supervisor mod

    logic isRrw; // is Register-Register
    logic isRr; // is Register-Register
    logic isIm; // is Immedate
    logic isLd; // is Load
    logic isSt; // is Store
    logic isBr; // is Branch
    logic isMul;// is Muliply instruction
    logic isSys;

	  logic [6:0] func7; //
	  logic [2:0] func3; //

    logic [XLEN-1:0] src1; // First source of data
    logic [XLEN-1:0] src2; // Second source of data
    logic [XLEN-1:0] src3; // Third source of data

    branchPrediction_t bp; // Branch prediction

    logic [$clog2(RFLEN)-1:0]  dst;   // Destination register
    logic                     vdst;  // Instruction writes to dst
    logic          [XLEN-1:0] ddst;  // Destination register data
    logic                     rdst;  // Data is ready to be writen

    logic [XLEN-1:0] excep;
    logic [XLEN-1:0] vpc;
  } decodedInstruction_t ; // TODO: change name to register readed instruction
    
endpackage


