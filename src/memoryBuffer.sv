
import config_pkg::*;

module memoryBuffer
(
  input  logic clk,
  input  logic res,

  rwBus.hl bus_i,
  rwBus.ll bus_o
);

  typedef struct {
    logic  [MLEN-1:0] addr;
    logic             ldp;
    logic             ldr;
    logic [MBLEN-1:0] ldData;
    logic             stp;
    logic             str;
    logic [MBLEN-1:0] stData;
  } bus_t;

  typedef enum { IDLE, BUSSY, ACK } state_t;

  state_t state[0:3];
  state_t nxState[0:3];

  bus_t buffer[0:3];
  bus_t nxBuffer[0:3];


  // Initial stage
  //
  assign bus_i.ldr = buffer[0].ldr;
  assign bus_i.str = buffer[0].str;
  assign bus_i.ldData = buffer[0].ldData;

  always_comb
  begin

    nxState[0] <= state[0];
    nxBuffer[0] <= buffer[0];
    nxBuffer[0].stData <= bus_i.stData;
    nxBuffer[0].ldData <= buffer[1].ldData;

    case(state[0])
      IDLE: begin
        if ( bus_i.ldp | bus_i.stp ) begin
          nxState[0]  <= BUSSY;
          nxBuffer[0].ldp    <= bus_i.ldp;
          nxBuffer[0].stp    <= bus_i.stp;
          nxBuffer[0].addr   <= bus_i.addr;
          nxBuffer[0].stData <= bus_i.stData;
        end
      end

      BUSSY: // BUSSY
        if ( buffer[1].ldr | buffer[1].str ) begin
          nxState[0] <= ACK;
          nxBuffer[0].ldr <= buffer[1].ldr;
          nxBuffer[0].str <= buffer[1].str;
        end

      default: // ACK
      begin
        nxBuffer[0].ldr    <= 0;
        nxBuffer[0].str    <= 0;
        if ( ~ (bus_i.ldp | bus_i.stp) ) begin
          nxState[0] <= IDLE;
          nxBuffer[0].ldp    <= 0;
          nxBuffer[0].stp    <= 0;
        end
      end
    endcase

    if ( res ) begin
      nxState[0] <= IDLE;
      nxBuffer[0].ldp    <= 0;
      nxBuffer[0].stp    <= 0;
      nxBuffer[0].ldr    <= 0;
      nxBuffer[0].str    <= 0;
    end
  end

  always_ff @(posedge clk)
  begin
    state[0]  = nxState[0];
    buffer[0] = nxBuffer[0];
  end


  // Last stage
  //
  assign bus_o.addr   = buffer[3].addr;
  assign bus_o.ldp    = buffer[3].ldp;
  assign bus_o.stp    = buffer[3].stp;
  assign bus_o.stData = buffer[3].stData;

  always_comb
  begin

    nxState[3]  <= state[3];
    nxBuffer[3] <= buffer[3];
    nxBuffer[3].ldData <= bus_o.ldData;

    case(state[3])
      IDLE: begin
        if ( buffer[2].ldp | buffer[2].stp ) begin
          nxState[3]  <= BUSSY;
          nxBuffer[3].ldp    <= buffer[2].ldp;
          nxBuffer[3].stp    <= buffer[2].stp;
          nxBuffer[3].addr   <= buffer[2].addr;
          nxBuffer[3].stData <= buffer[2].stData;
        end
      end

      BUSSY: // BUSSY
        if ( bus_o.ldr | bus_o.str ) begin
          nxState[3] <= ACK;
          nxBuffer[3].ldr <= bus_o.ldr;
          nxBuffer[3].str <= bus_o.str;
        end

      default: // ACK 
      begin
        nxBuffer[3].ldr <= 0;
        nxBuffer[3].str <= 0;
        if ( ~ (buffer[2].ldp | buffer[2].stp) ) begin
          nxState[3] <= IDLE;
          nxBuffer[3].ldp <= 0;
          nxBuffer[3].stp <= 0;
        end
      end
    endcase

    if ( res ) begin
      nxState[3] <= IDLE;
      nxBuffer[3].ldp    <= 0;
      nxBuffer[3].stp    <= 0;
      nxBuffer[3].ldr    <= 0;
      nxBuffer[3].str    <= 0;
    end
  end

  always_ff @(posedge clk)
  begin
    state[3]  = nxState[3];
    buffer[3] = nxBuffer[3];
  end

  // Middle stages

  genvar i;
  generate
    for(i=1;i<3;i++) begin
      always_comb
      begin

        nxState[i]  <= state[i];
        nxBuffer[i] <= buffer[i];
        nxBuffer[i].ldData <= buffer[i+1].ldData;

        case(state[i])
          IDLE: begin
            if ( buffer[i-1].ldp | buffer[i-1].stp ) begin
              nxState[i]  <= BUSSY;
              nxBuffer[i].ldp    <= buffer[i-1].ldp;
              nxBuffer[i].stp    <= buffer[i-1].stp;
              nxBuffer[i].addr   <= buffer[i-1].addr;
              nxBuffer[i].stData <= buffer[i-1].stData;
            end
          end

          BUSSY: 
            if ( buffer[i+1].ldr | buffer[i+1].str ) begin
              nxState[i] <= ACK;
              nxBuffer[i].ldr    <= buffer[i+1].ldr;
              nxBuffer[i].str    <= buffer[i+1].str;
            end

          default: // ACK
          begin
            nxBuffer[i].ldr    <= 0;
            nxBuffer[i].str    <= 0;
            if ( ~ (buffer[i-1].ldp | buffer[i-1].stp) ) begin
              nxState[i] <= IDLE;
              nxBuffer[i].ldp    <= 0;
              nxBuffer[i].stp    <= 0;
            end
          end

        endcase

        if ( res ) begin
          nxState[i] <= IDLE;
          nxBuffer[i].ldp    <= 0;
          nxBuffer[i].stp    <= 0;
          nxBuffer[i].ldr    <= 0;
          nxBuffer[i].str    <= 0;
        end
      end

      always_ff @(posedge clk)
      begin
        state[i]  = nxState[i];
        buffer[i] = nxBuffer[i];
      end
    end
  endgenerate



endmodule
