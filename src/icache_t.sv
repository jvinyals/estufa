
import config_pkg::*;

module icache_t
(
  input  logic clk,
  input  logic res,

  logic en,

  input  logic [MLEN-1:0] addr,

  output logic miss,
  output logic [ILEN-1:0] odata,

  roBus.ll bus
);

  // /__________TAG_____/__INDEX__/__OFFSET__/___00___/
  // /___[XLEN-1:15]____/_[14:5]__/__ [4:2]__/_[1:0]__/
  //

  localparam integer nCacheLines = 2;
  localparam integer nBitsIndex  = $clog2(nCacheLines);

  localparam integer nBytesILEN  = ILEN/8;
  localparam integer nMaskedBits = $clog2(nBytesILEN);

  localparam integer nBytesLine  = ICLLEN / 8;
  localparam integer nBitsOffset = $clog2(nBytesLine) - nMaskedBits;

  localparam integer nBitsTag    = MLEN - nBitsIndex - nBitsOffset - nMaskedBits;

  localparam integer offset_b = nMaskedBits;
  localparam integer offset_e = offset_b + nBitsOffset - 1;

  localparam integer index_b = offset_e + 1;
  localparam integer index_e = index_b  + nBitsIndex -1;

  localparam integer tag_b = index_e + 1;
  localparam integer tag_e = tag_b   + nBitsTag -1;

  initial 
    assert (tag_e != MLEN-1) $error("Wrong tag calculation. tag_e = " + tag_e);


  logic                       valid [ICLEN-1:0];
  logic      [nBitsTag-1:0]    tags [ICLEN-1:0];
  logic        [ICLLEN-1:0]    data [ICLEN-1:0];

  logic                     nxValid [ICLEN-1:0];
  logic      [nBitsTag-1:0]  nxTags [ICLEN-1:0];
  logic        [ICLLEN-1:0]  nxData [ICLEN-1:0];

  logic                selMiss;
  logic [nBitsTag-1:0] selTag;
  logic     [ILEN-1:0] selData;

  logic              [nBitsIndex-1:0] index;
  logic             [nBitsOffset-1:0] offset;
  logic [(ICLLEN/ILEN)-1:0][ILEN-1:0] selLine;

  assign odata = selData;
  assign offset  = addr_useful[offset_e:offset_b];
  assign index   = addr_useful[ index_e: index_b];
  
  logic [nCacheLines-1:0] lineSelector;
  assign lineSelector = en ? 1 << index : 0;

  genvar i;

  generate
    for (i = 0; i < ICLEN; i++)
    begin : cache_line_control

      always_comb
      begin

        nxValid[i] <= valid[i];
        nxTags[i]  <= tags[i]; 
        nxData[i]  <= data[i]; 

        if (res) begin
          nxValid[i] <= 0;
          nxTags[i]  <= {nBitsTag{1'd0}}; 
          nxData[i]  <=   {ICLLEN{1'd0}}; 
        end else if (bus.ldr /*ibusEN*/) begin
          if (lineSelector[i]) begin 
            nxValid[i] <= 1;
            nxTags[i]  <= addr_useful[tag_e:tag_b];
            nxData[i]  <= bus.ldData /*ibus*/;
          end
        end
      end

      always_ff @(posedge clk)
      begin
        valid[i] <= nxValid[i];
        tags[i]  <= nxTags[i];
        data[i]  <= nxData[i];
      end

    end
  endgenerate


  always_comb
  begin
    selTag  = tags[index];
    selLine = data[index];
    selData = selLine[offset];
  end

  always_comb
    selMiss = selTag != addr_useful[tag_e:tag_b] || ~valid[index];

  typedef enum {IDLE, LDP} bus_state_t;
  bus_state_t nxState, state;

  always_comb
  begin
    case(state)
      IDLE: miss = selMiss & en;
      LDP:  miss = 1;
    endcase
  end

  always_comb
  begin
    nxState <= state;
    nx_addr_ff <= addr_ff;
    case(state)
      IDLE: if (miss) begin
        nxState <= LDP;
        nx_addr_ff <= addr;
      end

      LDP: if(bus.ldr) begin
        nxState <= IDLE;
      end
    endcase
  end

  always_ff @(posedge clk)
  begin 
    state = nxState;
    addr_ff = nx_addr_ff;
  end

  always_comb
  begin
    case(state)
      IDLE: addr_useful = addr;
      LDP:  addr_useful = addr_ff;
    endcase
  end

  assign bus.addr = {addr_useful[MLEN-1:4], 4'b0};
  assign bus.ldp  = miss;
endmodule 
