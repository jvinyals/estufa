
import config_pkg::*;
import instruction_pkg::*;
import tlb_pkg::*;

module memoryStage
(
  input  logic clk,
  input  logic res,

  input  logic                     glen,
  input  logic [$clog2(GLLEN)-1:0] glid,

  input  logic flush_tlb,

  input  logic stall_i,
  output logic stall_o,

  input  decodedInstruction_t instruction_i,
  output decodedInstruction_t instruction_o,

  rwBus.ll dataBUS,

  pwBus.ll pageBUS
);

  logic [XLEN-1:0] vaddr;
  assign vaddr = instruction_i.ddst;

  logic [MLEN-1:0] paddr;
  assign paddr = vaddr[MLEN-1:0]; // TODO: create a TLB

  logic [2:0] func3;
  assign func3 = instruction_i.func3;

  localparam integer indexBytes_e = $clog2(XLEN/8)-1;
  localparam integer indexBytes_b = 0;

  integer offset_bytes;
  assign offset_bytes = paddr[indexBytes_e:indexBytes_b];
  logic [(XLEN/8)-1:0] mem_mask;
  logic [(XLEN/8)-1:0] store_mask;
  assign store_mask = mem_mask & {(XLEN/8){instruction_i.isSt}};

  always_comb
  begin
    case(func3)
      0: mem_mask = 1  << offset_bytes;
      1: mem_mask = 3  << offset_bytes;
      2: mem_mask = 7  << offset_bytes;
      3: mem_mask = 15 << offset_bytes;
    endcase
  end

  logic [XLEN-1:0] stData;
  logic [XLEN-1:0] ldData;
  logic [XLEN-1:0] ldData_raw;

  logic cache_miss;
  logic enable_cache;
  assign enable_cache = instruction_i.valid | instruction_i.isLd | instruction_i.isSt;

  always_comb
  begin
    stData = instruction_i.src3 << offset_bytes;
  end

  dcache_t dmem (
    .clk,
    .res,

    .en(enable_cache),

    .addr(paddr),
    .wren(store_mask),
    .odata(ldData_raw),
    .idata(stData),

    .miss(cache_miss), // TODO: repair signal

    .bus(dataBUS)
  );

  logic [63:0] ldData_d;
  logic [31:0] ldData_w;
  logic [15:0] ldData_h;
  logic [ 7:0] ldData_b;

  logic [XLEN-1:0] ldData_ds, ldData_du;
  logic [XLEN-1:0] ldData_ws, ldData_wu;
  logic [XLEN-1:0] ldData_hs, ldData_hu;
  logic [XLEN-1:0] ldData_bs, ldData_bu;

  logic [XLEN-1:0] ldData_off;
  integer offset_bits;
  assign offset_bits = offset_bytes * 8;
  assign ldData_off = (ldData_raw >> offset_bits);

  assign ldData_d = ldData_off[63:0];
  assign ldData_w = ldData_off[31:0];
  assign ldData_h = ldData_off[15:0];
  assign ldData_b = ldData_off[ 7:0];

  assign ldData_ds = { ldData_d };
  assign ldData_du = { ldData_d };

  assign ldData_ws = { {32{ldData_b[7]}}, ldData_w };
  assign ldData_wu = {        {32{1'b0}}, ldData_w };

  assign ldData_hs = { {48{ldData_b[7]}}, ldData_h };
  assign ldData_hu = {        {48{1'b0}}, ldData_h };

  assign ldData_bs = { {56{ldData_b[7]}}, ldData_b };
  assign ldData_bu = {        {56{1'b0}}, ldData_b };

  always_comb
  begin
    case(func3)
      0: ldData = ldData_bs;
      1: ldData = ldData_hs;
      2: ldData = ldData_ws;
      3: ldData = ldData_ds;
      4: ldData = ldData_bu;
      5: ldData = ldData_hu;
      6: ldData = ldData_wu;
    endcase
  end

  always_comb
  begin
    instruction_o <= instruction_i;

    instruction_o.rdst <= instruction_i.isLd | instruction_i.isSt;

    if(instruction_o.rdst) begin
      instruction_o.ddst <= ldData;
    end
  end

  // TODO: fix it
  assign stall_o = 0 || (cache_miss & (instruction_i.isLd | instruction_i.isSt) & instruction_i.valid);

endmodule
