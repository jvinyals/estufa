
import config_pkg::*;
import branch_pkg::*;
import instruction_pkg::*;

module pipeline 
(
  input  logic clk,
  input  logic res,

  roBus.ll instBUS,
  rwBus.ll dataBUS,
  roBus.ll pageBUS
);

  logic stallFet;
  logic stallDec, stallDec_o;
  logic stallALU, stallALU_o;
  logic stallMEM, stallMEM_o;
  logic stallMS,  stallMS_o;
  assign stallMS = 0;


  logic killDec, killDec_o;
  logic killALU, killALU_o;
  logic killMEM, killMEM_o;
  logic killMS;

  evaluatedBranch_t evalBR;

  logic [ILEN-1:0] irFetch, nxir, ir;

  logic sret_en;

  // Exception @
  logic            csr0_mod;
  logic [XLEN-1:0] csr0_data;

  // SATP
  logic            csr1_mod;
  logic [XLEN-1:0] csr1_data;

  // Exception type
  logic            csr3_mod;
  logic [XLEN-1:0] csr3_data;

  // Supervisor/User mode
  logic            csr4_mod;
  logic [XLEN-1:0] csr4_data;

  // Move to package

  //                Stage Inst, Next Inst, Stage out inst
  codedInstruction_t                       instFet_o;
  codedInstruction_t   instDec, nxInstDec;
  decodedInstruction_t                     instDec_o;
  decodedInstruction_t instALU, nxInstALU, instALU_o;
  decodedInstruction_t instMEM, nxInstMEM, instMEM_o; 
  decodedInstruction_t instWB,  nxInstWB;

  decodedInstruction_t instMS,  nxInstMS, instMS_o;
  decodedInstruction_t   instMS_ff[MSLEN-1:0];
  decodedInstruction_t nxInstMS_ff[MSLEN-1:0];
  
  // Propagate kill signal backwards
  assign killMEM = killMEM_o;
  assign killALU = killMEM  | evalBR.miss;
  assign killDec = killALU  | sret_en | killDec_o;
  assign killFet = killDec  | sret_en;
    
  // Propagate stall signal backwards
  assign stallMEM = stallMEM_o;
  assign stallALU = stallMEM;
  assign stallDec = (stallMS  &  instDec_o.isMul ) | 
                    (stallALU & ~instDec_o.isMul ) |
                    (~instDec_o.isMul & instMS_ff[MSLEN-2].valid) |
                    stallDec_o;

  assign stallFet = stallDec;

  logic glen;
  logic [$clog2(GLLEN)-1:0] glid;

  assign glid = instWB.glid;
  assign glen = 1;

  // From GL to csrBUffer
  logic                     gl_wr_csr_en;
  logic              [11:0] gl_wr_csr_addr;
  logic          [XLEN-1:0] gl_wr_csr_data;


  // Compute pipe line next instruction for each stage
  //
  // Artificially extending the Multiplication stage length
  genvar i;
  generate

    for(i = 1; i < MSLEN; i++) begin : ms_ff_cotrol
      always_comb
      begin
        if (stallMS) nxInstMS_ff[i] <= instMS_ff[i];
        else         nxInstMS_ff[i] <= instMS_ff[i-1]; 

        // TODO: convert killMS and analogous to kill (from the graduation
        //       list)
        if (res | killMS) nxInstMS_ff[i].valid <= 0;
      end

      always_ff @(posedge clk)
        instMS_ff[i] = nxInstMS_ff[i];
    end

  endgenerate
  //
  //
  always_comb
  begin
    nxInstDec <= (stallDec ? instDec : instFet_o);
    nxInstALU <= (stallALU ? instALU : instDec_o);
    nxInstMEM <= (stallMEM ? instMEM : instALU_o);
    nxInstWB  <= instMEM_o;

    // Kill when signal kill is on and when we are the head of a stall
    if (instMEM_o.valid) begin
      nxInstWB <= instMEM_o;
      if (res | killMEM | stallMEM) nxInstWB.valid  <= 0;
    end else begin
      nxInstWB <= instMS_o ;
      if (res | killMS ) nxInstWB.valid  <= 0;
    end
    if (res | killMS )  nxInstMS.valid  <= 0;
    if (res | killMS  | stallDec & ~stallMS)  nxInstMS_ff[0].valid  <= 0;
    if (res | killALU | stallALU & ~stallMEM) nxInstMEM.valid <= 0;
    if (res | killDec | stallDec & ~stallALU) nxInstALU.valid <= 0;
    if (res | killFet | stallFet & ~stallDec) nxInstDec.valid <= 0;
  end

  always_ff @(posedge clk)
  begin
    instDec = nxInstDec;
    instALU = nxInstALU;
    instMEM = nxInstMEM;
    instWB  = nxInstWB;

    instMS  = nxInstMS;
    instMS_ff[0] = nxInstMS_ff[0];
  end

  pwBus itlb();
  pwBus dtlb();

  logic  intr;
  assign intr = 0; // TODO: add interruptions
  // Fetch
  fetchStage fetchStage_0
  (
    // Reset & clock
    .clk,
    .res,

    .su(csr4_data[0]),

    // Stage data
    .instruction_o(instFet_o),      // Next Instruction

    // Pipeline correction
    // stalling
    .stall_i(stallFet),

    .eb(evalBR),         // Evaluated branch prediction

    // killing
    .intr(intr),         // Interruption

    .sret(sret_en),
    .csr0_data,

    // Upper i-chache level
    .instBUS,

    // Upper page walker bus
    .flush_tlb(csr1_mod),
    .pageBUS(itlb)

  );

  // Decode
  decodeStage decodeStage_0 (
    // Reset & clock
    .clk,
    .res,

   .instruction_i(instDec),
   .instruction_o(instDec_o), // Decoded Instruction
	 .instruction_wb(instWB),

   // Pipeline control
   .stall_o(stallDec_o),

   .sret_en,
   .csr3_data,

   // Bypasses
   .bypassALU(instALU_o),      // ALU's stage instruction
   .bypassMEM(instMEM_o)       // MEM's stage instruction
  );

  // ALU
  aluStage aluStage_0 (
    .instruction_i(instALU),
    .instruction_o(instALU_o),
    .evaluatedBranch(evalBR)
  );

  // MEM
  memoryStage memStage_0 (
    .clk,
    .res,

    .instruction_i(instMEM),
    .instruction_o(instMEM_o),

    .stall_i(stallMEM),
    .stall_o(stallMEM_o),

    // Upper d-cache level
    .dataBUS
  );

endmodule 
