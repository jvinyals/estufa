
import config_pkg::*;
import branch_pkg::*;
import instruction_pkg::*;

module decodeStage
(
  input  logic clk,
  input  logic res,

  //input  logic [ILEN-1:0] ir,
  //input  branchPrediction_t bp,

  input  codedInstruction_t   instruction_i,
  output decodedInstruction_t instruction_o,
  input  decodedInstruction_t instruction_wb,

  output logic stall_o,

  output logic sret_en,

  input logic [XLEN-1:0] csr3_data,

  input  decodedInstruction_t bypassALU,
  input  decodedInstruction_t bypassMEM
);

  logic dependency;
  assign stall_o = dependency;

  decodedInstruction_t io;
  assign instruction_o = io;
  assign io.bp = instruction_i.bp;

  logic  [4:0] sr1Addr;
  logic [XLEN-1:0] sr1Data;

  logic  [4:0] sr2Addr;
  logic [XLEN-1:0] sr2Data;

  logic [ILEN-1:0] ir;
  assign ir = instruction_i.inst;

  logic [4:0] regAddr;

  logic [6:0] opcode;
  assign opcode = ir[6:0];

  assign io.valid = instruction_i.valid & ~dependency;
  assign io.isIm  = opcode == 7'h13 | opcode == 7'h37 ? io.valid : 0;
  assign io.isRr  = opcode == 7'h33 ? io.valid : 0;
  assign io.isRrw = opcode == 7'h3b ? io.valid : 0;
  assign io.isLd  = opcode == 7'h03 ? io.valid : 0;
  assign io.isSt  = opcode == 7'h23 ? io.valid : 0;
  assign io.isBr  = opcode == 7'h63 ? io.valid : 0;
  assign io.isMul = opcode == 7'h33 ? io.valid : 0;
  assign io.isSys = opcode == 7'h73 ? io.valid : 0;

  assign sr1Addr  = opcode == 7'h37 ? 0 : ir[19:15];
  assign sr2Addr  = ir[24:20];
  assign regAddr = ir[11: 7];
  assign io.dst  = regAddr;
  assign io.ddst = io.rdst ? csr3_data : 32'h0;
  assign io.rdst = (io.isSys && io.func3 == 1);

  assign io.func7 = ir[31:25];
  assign io.func3 = opcode == 7'h37 ? 0 : ir[14:12];

  assign io.su = instruction_i.su;
  assign io.excep = instruction_i.excep;
  assign io.vpc   = instruction_i.vpc;
  
  // TODO: change to the renaming stage
  assign io.glid = 0;

  // if func3 is 0 sret
  // if func3 is 1 csrw
  // if func3 is 2 csrr
  // 01110011
  // csrrs     rd      rs1      imm12        14..12=2 6..2=0x1C 1..0=3
  // csrrw     rd      rs1      imm12        14..12=1 6..2=0x1C 1..0=3
  // sret      11..7=0 19..15=0 31..20=0x102 14..12=0 6..2=0x1C 1..0=3
  assign sret_en = io.isSys && io.func3 == 0;

  assign io.vdst = ~dependency & ( io.isRr | io.isRrw | io.isIm | io.isLd | (io.isSys && io.func3 == 1) );

  logic [XLEN-1:0] immediate;

  always_comb
  begin
		case(opcode[6:2])
      5'h18: immediate = { {XLEN-12{ir[31]}}, ir[7], ir[30:25], ir[11:8], 1'b0 };
			5'h04: immediate = { {XLEN-12{ir[31]}}, ir[31:20] };
			5'h0C: immediate = {XLEN{1'd0}};
			5'h00: immediate = { {XLEN-12{ir[31]}}, ir[31:20] };
			5'h08: immediate = { {XLEN-12{ir[31]}}, ir[31:25], ir[11:7]};
      5'h0D: immediate = { 12'b0, ir[31:12] };
      5'h1C: immediate = { {XLEN-12{1'b0}}, ir[31:20] } ;
			default: immediate = {XLEN{1'd0}};
		endcase
  end

  logic  enable_rf_write;
  assign enable_rf_write = instruction_wb.vdst & instruction_wb.valid;

  regfile_t regfile_0 (
    .clk,
    .res,

    .sr1Addr,
    .sr1Data,

    .sr2Addr,
    .sr2Data,

    .drWrEn(enable_rf_write),
    .drAddr(instruction_wb.dst),
    .drData(instruction_wb.ddst)
  );

  logic [XLEN-1:0] bpSr1Data, bpSr2Data;

  logic dpSr1ALU, dpSr2ALU; // Detect dependency with ALU
  assign dpSr1ALU = (bypassALU.dst == sr1Addr) & bypassALU.vdst & bypassALU.valid;
  assign dpSr2ALU = (bypassALU.dst == sr2Addr) & bypassALU.vdst & bypassALU.valid;
  assign dpDstALU = (bypassALU.dst == regAddr) & bypassALU.vdst & bypassALU.valid;

  logic dpSr1MEM, dpSr2MEM; // Detect dependency with MEM
  assign dpSr1MEM = (bypassMEM.dst == sr1Addr) & bypassMEM.vdst & bypassMEM.valid;
  assign dpSr2MEM = (bypassMEM.dst == sr2Addr) & bypassMEM.vdst & bypassMEM.valid;
  assign dpDstMEM = (bypassMEM.dst == regAddr) & bypassMEM.vdst & bypassMEM.valid;

  logic dpSr1, dpSr2, dpDst;

  always_comb
  begin
    dpSr1 <= 0;
    dpSr2 <= 0;
    dpDst <= 0;

    // Default data if no bypass is avaliable
    bpSr1Data <= sr1Data;
    bpSr2Data <= sr2Data;

    // TODO: check if instruction needs two registers sr1 and sr2
    // Bypass from MEM
    if (bypassMEM.valid) begin
      if (bypassMEM.rdst) begin
        if (dpSr1MEM) begin 
          bpSr1Data <= bypassMEM.ddst;
          dpSr1     <= 0;
        end
        if (dpSr2MEM) begin
          bpSr2Data <= bypassMEM.ddst;
          dpSr2     <= 0;
        end
      end else begin
        if (dpSr1MEM) dpSr1 <= 1;
        if (dpSr2MEM) dpSr2 <= 1;
        if (dpDstMEM) dpDst <= 0; // TODO: remove WAW dependency control
      end
    end

    // Bypass from ALU (overrides MEM if necessary)
    if (bypassALU.valid) begin
      if (bypassALU.rdst) begin
        if (dpSr1ALU) begin
          bpSr1Data <= bypassALU.ddst;
          dpSr1     <= 0;
        end
        if (dpSr2ALU) begin
          bpSr2Data <= bypassALU.ddst;
          dpSr2     <= 0;
        end
      end else begin
        if (dpSr1ALU) dpSr1 <= 1;
        if (dpSr2ALU) dpSr2 <= 1;
        if (dpDstALU) dpDst <= 0; // TODO: remove WAW dependency control
      end
    end
  end
 
  assign dependency = dpSr1 | dpSr2 | dpDst;

	always_comb
	begin

    // Set nxA
    if (io.isSys && io.func3 == 2) io.src1 <= csr3_data;
    else                           io.src1 <= bpSr1Data;

    // Set nxB
    io.src2 <= bpSr2Data;
    if (io.isSys && (io.func3 == 2 | io.func3 == 1))
      io.src2 <= immediate;
    if (io.isIm | io.isLd | io.isSt) io.src2 <= immediate;

    // Set nxC
    io.src3 = immediate; // Not used in most instructions
    if (io.isSt) io.src3 <= bpSr2Data;
	end


endmodule
