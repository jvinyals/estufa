
import config_pkg::*;

module controlStateRegisterFile
(
  input logic clk,
  input logic res,

  output logic [CSRLEN-1:0][XLEN-1:0] csrData,

  input  logic            wrEn,
  input  logic     [11:0] wrAddr,
  input  logic [XLEN-1:0] wrData
);

  logic  nclk;
  assign nclk = ~clk;
  logic [XLEN-1:0]    registers [CSRLEN-1:0];
  logic [XLEN-1:0] nxregegister [CSRLEN-1:0];

  assign {>>{csrData}} = registers;

  genvar i;
  generate 

    for(i=0; i < CSRLEN; i++) 
    begin : regfile_control
      always_comb
      begin
        if (res) 
          if (i == 1'h04) // Super/user register starts at supervisor mode
            nxregegister[i] = 1'd1;
          else
            nxregegister[i] = 1'd0;
        else if (i == wrAddr && wrEn)
            nxregegister[i] = wrData;
        else
          nxregegister[i] = registers[i];
      end

      always_ff @(posedge nclk)
        registers[i] = nxregegister[i];

    end
  endgenerate

endmodule
