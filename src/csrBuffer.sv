
import config_pkg::*;

module csrBuffer
(
  input logic clk,
  input logic res,

  input  logic             glen,
  input  logic [$clog2(GLLEN)-1:0] glid,

  output logic full,

  input  logic             en_i,
  input  logic [$clog2(GLLEN)-1:0] glid_i,
  input  logic      [11:0] addr_i,
  input  logic  [XLEN-1:0] data_i,

  output logic             en_o,
  output logic [$clog2(GLLEN)-1:0] glid_o,
  output logic      [11:0] addr_o,
  output logic  [XLEN-1:0] data_o
);

  parameter BUFFLEN = 16;

  logic [$clog2(BUFFLEN)-1:0] tail, nxTail;


  typedef struct{
    logic            valid;
    logic      [11:0] addr;
    logic  [XLEN-1:0] data;
    logic [$clog2(GLLEN)-1:0] glid;
  }csr_buff_entry_t ;

  csr_buff_entry_t   buff[BUFFLEN:0];
  csr_buff_entry_t nxBuff[BUFFLEN-1:0];

  always_ff @(posedge clk) 
  begin
    buff[BUFFLEN].valid = 0;
    buff[BUFFLEN].addr  = 0;
    buff[BUFFLEN].data  = 0;
    buff[BUFFLEN].glid  = 0;

    buff[BUFFLEN-1:0] = nxBuff;
  end

  logic  enable_drain; // When we drain from the last entry of the csr table.
  assign enable_drain = buff[0].valid && buff[0].glid == glid && glen;
  assign en_o   = enable_drain;
  assign addr_o = buff[0].addr;
  assign data_o = buff[0].data;
  assign glid_o = buff[0].glid;

  assign full = tail == BUFFLEN && ~enable_drain;

  logic [$clog2(BUFFLEN)-1:0] write_tail;

  always_comb
  begin
    if (enable_drain) write_tail = tail - 1;
    else              write_tail = tail;
  end

  always_comb
  begin
    if (enable_drain) begin 
      if (en_i) nxTail <= write_tail + 1;
      else      nxTail <= write_tail;
    end else begin
      if (en_i) nxTail <= tail + 1;
      else      nxTail <= tail;
    end
    if(res) nxTail <= 0;
  end

  genvar i;
  generate 

    for(i=0; i<BUFFLEN; i++) 
    begin : csr_buff_entry_control
      always_comb
      begin
        if (enable_drain) begin
          nxBuff[i] <= buff[i+1];
        end else begin
          nxBuff[i] <= buff[i];
        end

        if (en_i && i == write_tail) begin
          nxBuff[i].valid <= 1;
          nxBuff[i].addr  <= addr_i;
          nxBuff[i].data  <= data_i;
          nxBuff[i].glid  <= glid_i;
        end

        if (res) begin 
          buff[i].valid = 0;
          buff[i].addr  = 0;
          buff[i].data  = 0;
          buff[i].glid  = 0;
        end
      end
    end
  endgenerate

endmodule
